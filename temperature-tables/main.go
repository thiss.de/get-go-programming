package main

import (
	"fmt"
	"strings"

	"thiss.de/temperature-tables/tables"
)

func main() {
	fmt.Println("Welcome to the Temperature Table")
	drawTable("ºC", "ºF", celsiusToFahrenheit)

	drawTable("ºF", "ºC", fahrenheitToCelsius)

	tables.Run()
}

// drawTable prints a temperature table in the console
func drawTable(from string, to string, convertTemperature func(float64) float64) {
	fmt.Println(strings.Repeat("=", 23))
	fmt.Printf("|%-10s|%-10s|\n", from, to)
	fmt.Println(strings.Repeat("=", 23))

	for t := -40; t <= 100; t += 5 {
		fmt.Printf("|%10.2f|%10.2f|\n", float64(t), convertTemperature(float64(t)))
	}
	fmt.Println(strings.Repeat("=", 23))
}

func celsiusToFahrenheit(c float64) float64 {
	return (c * 9.0 / 5.0) + 32
}

func fahrenheitToCelsius(f float64) float64 {
	return (f - 32.0) * 5.0 / 9.0
}
