package main

import (
	"fmt"
	"math"
	"math/big"
	"math/rand"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func main() {
	rangeAgain(true)
	cipher(false)
	decipher(false)
	confirmationToBool(false)
	convert(false)
	mixedTypes(false)
	rot13international(false)
	caesarCipher(false)
	spanish(false)
	rot13(false)
	stringArray(false)
	runeType(false)
	rawStrings(false)
	andromeda(false)
	distandMarsEarth(false)
	alpha(false)
	betterPiggyBank(false)
	hexaDecimalNumbers(false)
	typePrint(false)
	piNumbers(false)
	savingMoney(false)
}

func rangeAgain(run bool) {
	if !run {
		return
	}
	for pos, char := range "日本\x80語" { // \x80 is an illegal UTF-8 encoding
		fmt.Printf("character %#U starts at byte position %d\n", char, pos)
	}
}

func cipher(run bool) {
	if !run {
		return
	}
	cipherText := "who am I to talk about this"
	keyword := "GOLANG"
	message := ""
	keyIndex := 0

	cipherText = strings.Replace(cipherText, " ", "", -1)
	cipherText = strings.ToUpper(cipherText)

	for i := 0; i < len(cipherText); i++ {
		c := cipherText[i] - 'A'
		k := keyword[keyIndex] - 'A'

		c = (c+k+26)%26 + 'A'
		message += string(c)

		keyIndex++
		keyIndex %= len(keyword)

	}
	fmt.Println(message)
}

func decipher(run bool) {
	if !run {
		return
	}
	cipherText := "CSOITEUIWUIZNSROCNKFD"
	cipherText = "CVZAZOZCEAYQGPZUGZNWD"
	keyword := "GOLANG"
	message := ""
	keyIndex := 0

	for i := 0; i < len(cipherText); i++ {
		c := cipherText[i] - 'A'
		k := keyword[keyIndex] - 'A'

		c = (c-k+26)%26 + 'A'
		message += string(c)

		keyIndex++
		keyIndex %= len(keyword)

	}
	fmt.Println(message)
}

func confirmationToBool(run bool) {
	if !run {
		return
	}
	vals := [8]string{"true", "yes", "1", "bar", "false", "no", "0", "foo"}
	var r bool
	for i := 0; i < len(vals); i++ {
		v := vals[i]
		switch v {
		case "true", "yes", "1":
			r = true
		case "false", "no", "0":
			r = false
		default:
			fmt.Printf("Can't convert %v to bool\n", v)
			continue
		}
		fmt.Printf("%v as bool: %v\n", v, r)
	}
}

func convert(run bool) {
	if !run {
		return
	}
	root := "10"
	countdown, err := strconv.Atoi(root)
	if err != nil {
		fmt.Printf("Couldn't convert %v to integer. Error:\n%v\n", root, err)
	}
	fmt.Println(countdown)

	launch := false
	launchText := fmt.Sprintf("%v", launch)
	fmt.Println("Ready for launch:", launchText)

	var yesNo string
	if launch {
		yesNo = "yes"
	} else {
		yesNo = "no"
	}
	fmt.Println("Ready for launch:", yesNo)

	yesNo = "no"

	launch = (yesNo == "yes")
	fmt.Println("Ready for launch:", launch)

}

func mixedTypes(run bool) {
	if !run {
		return
	}
	age := 42
	marsDays := 687.0
	earthDays := 365.2425
	fmt.Println("I am", age*int(earthDays)/int(marsDays), "years old on Mars.")
	marsAge := float64(age)
	marsAge = marsAge * earthDays / marsDays
	fmt.Printf("I am %2.3f years old on Mars.\n", marsAge)

	roundUp := 3.8
	roundDown := 3.2
	fmt.Printf("The int(float64) function truncates: %T %v to %v, %T %v to %v\n", roundUp, roundUp, int(roundUp), roundDown, roundDown, int(roundDown))
}

func rot13international(run bool) {
	if !run {
		return
	}

	message := "uv vagreangvbany fcnpr fgngvba"
	message = "hi international space station"
	message = "¿Cómo estás?"
	message = "¿Cózb rfgáf?"

	for _, c := range message {
		if c >= 'a' && c <= 'z' {
			c = c + 13
			if c > 'z' {
				c -= 26
			}
		}
		fmt.Printf("%c", c)
	}
	fmt.Printf("\n")
}

func caesarCipher(run bool) {
	if !run {
		return
	}
	message := "L fdph, L vdz, L frqtxhuhg."

	for i := 0; i < len(message); i++ {
		c := message[i]
		if (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') {
			c = c - 3
			if c > 'z' {
				c -= 26
			}
		}
		fmt.Printf("%c", c)
	}
	fmt.Printf("\n")
}

func spanish(run bool) {
	if !run {
		return
	}
	question := "¿Cómo estás?"
	fmt.Println(len(question), "bytes")
	fmt.Println(utf8.RuneCountInString(question), "runes")

	c, size := utf8.DecodeRuneInString(question)
	fmt.Printf("First rune: %c %v bytes\n", c, size)

	for i, c := range question {
		fmt.Printf("%v %c\n", i, c)
	}

	for _, c := range question {
		fmt.Printf("%c ", c)
	}
	fmt.Printf("\n")
}

func rot13(run bool) {
	if !run {
		return
	}

	message := "uv vagreangvbany fcnpr fgngvba"
	fmt.Println(message)
	message = "hi international space station"

	for i := 0; i < len(message); i++ {
		c := message[i]
		if c >= 'a' && c <= 'z' {
			c = c + 13
			if c > 'z' {
				c -= 26
			}
		}
		fmt.Printf("%c", c)
	}
	fmt.Printf("\n")
}

func stringArray(run bool) {
	if !run {
		return
	}
	s := "shalom"

	for index := 0; index < len(s); index++ {
		fmt.Printf("%d nth character is: %c\n", index, s[index])
	}
}

func runeType(run bool) {
	if !run {
		return
	}
	var pi rune = 960
	var alpha rune = 940
	var omega rune = 969
	var bang byte = 33

	fmt.Printf("%v %v %v %v\n", pi, alpha, omega, bang)

	fmt.Printf("%c%c%c%c\n", pi, alpha, omega, bang)

	grade1 := 'A'
	var grade2 = 'A'
	var grade3 rune = 'A'

	fmt.Printf("Walrus typed [%v, %T], auto typed [%v, %T], rune type [%v, %T]\n",
		grade1, grade1, grade2, grade2, grade3, grade3)

	var star byte = '*'
	fmt.Printf("star: %c, %v, %d\n", star, star, star)
}

func rawStrings(run bool) {
	if !run {
		return
	}

	fmt.Println("peace can be upon you\nupon you be peace")
	fmt.Println(`strings can span multiple lines with the \n escape sequence`)

	fmt.Println(`
	peace can be upon you
	upon you be peace`)

	fmt.Printf("%v is a %[1]T\n", "literal string")

	fmt.Printf("%v is a %[1]T\n", `raw string literal`)
}

func andromeda(run bool) {
	if !run {
		return
	}

	lightSpeed := big.NewInt(299792)
	secondsPerDay := big.NewInt(60 * 60 * 24)
	distance := new(big.Int)
	distance.SetString("24000000000000000000", 10)
	fmt.Println("Andromeda Galaxy is", distance, "km away.")

	seconds := new(big.Int)
	seconds.Div(distance, lightSpeed)

	days := new(big.Int)
	days.Div(seconds, secondsPerDay)

	fmt.Println("That is", days, "days of travel at light speed.")

	p := message.NewPrinter(language.Make("de"))
	p.Printf("That is %v days of travel at light speed.\n", days)

}

func distandMarsEarth(run bool) {
	if !run {
		return
	}
	var rangeMin int64 = 56e6
	var rangeMax int64 = 401e6
	fmt.Printf("The minimum distance between Mars and Earth is %v\n", rangeMin)
	fmt.Printf("The maximum distance between Mars and Earth is %v\n", rangeMax)

	p := message.NewPrinter(language.Make("de"))
	p.Printf("The maximum distance between Mars and Earth is %v\n", rangeMax)

	magicNumber := 5.6e7
	fmt.Printf("Type of %v is %T\n", magicNumber, magicNumber)
}

func alpha(run bool) {
	if !run {
		return
	}
	const lightSpeed = 299792
	const secondsPerDay = 60 * 60 * 24

	var distance int64 = 41.3e12
	fmt.Printf("Alpha Centauri is %v km away.\n", distance)

	days := distance / lightSpeed / secondsPerDay
	fmt.Printf("That is %v days of travel at light speed.\n", days)
}

func betterPiggyBank(run bool) {
	if !run {
		return
	}
	giftMoney := 0

	for giftMoney <= 20*100 {
		t := time.Now()
		rand.Seed(int64(t.Nanosecond()))
		var choice = rand.Intn(3)
		input := 0
		switch choice {
		case 0:
			input = 5
		case 1:
			input = 10
		default:
			input = 25
		}
		giftMoney += input
		fmt.Printf("Putting %02d¢ into the box, you now have %02d.%02d$ saved\n",
			input, giftMoney/100, giftMoney%100)
	}
}

func hexaDecimalNumbers(run bool) {
	if !run {
		return
	}
	var r, g, b uint8 = 0, 141, 213
	fmt.Printf("%x %x %x\n", r, g, b)
	fmt.Printf("color: #%02x%02x%02x;\n", r, g, b)

	var red, green, blue uint8 = 0x00, 0x8d, 0xd5
	fmt.Printf("%x %x %x\n", red, green, blue)
	fmt.Printf("color: #%02x%02x%02x;\n", red, green, blue)

	var num uint8 = 250
	for i := 0; i < 10; i++ {
		fmt.Printf("the number %v in bits: %08b\n", num, num)
		num++
	}
}

func typePrint(run bool) {
	if !run {
		return
	}
	var year = 2021
	fmt.Printf("Type %T for %v\n", year, year)

	days := 365.2425
	fmt.Printf("Type %T for %[1]v\n", days, days)

	text := "Text between quotes"
	fmt.Printf("Type %T for %v\n", text, text)

	whatType := true
	fmt.Printf("Type %T for %v\n", whatType, whatType)
}

func piNumbers(run bool) {
	if !run {
		return
	}
	var pi64 = math.Pi
	var pi32 float32 = math.Pi

	fmt.Println("64 bit float pi:", pi64)
	fmt.Println("32 bit float pi:", pi32)

	third := 1.0 / 3
	fmt.Println(third)
	fmt.Printf("%v\n", third)
	fmt.Printf("%f\n", third)
	fmt.Printf("%.3f\n", third)
	fmt.Printf("%010.2f\n", third)

	fmt.Println(third + third + third)

	piggyBank := 0.1
	piggyBank += 0.2
	fmt.Println(piggyBank)
	fmt.Println(piggyBank == 0.3)
	fmt.Println(math.Abs(piggyBank-0.3) < 0.0001)

	piggyBank = 0.0
	for i := 0; i < 11; i++ {
		piggyBank += 0.1
	}
	fmt.Println(piggyBank)

	celsius := 21.0
	fmt.Print((celsius/5.0*9.0)+32, " º F\n")
	fmt.Print((9.0/5.0*celsius)+32, " º F\n")

	fahrenheit := (celsius * 9.0 / 5.0) + 32
	fmt.Print(fahrenheit, " º F\n")
}

func savingMoney(run bool) {
	if !run {
		return
	}
	giftMoney := 0.0

	for giftMoney < 20.0 {
		t := time.Now()
		rand.Seed(int64(t.Nanosecond()))
		var choice = rand.Intn(3)
		input := 0.0
		switch choice {
		case 0:
			input = 0.05
		case 1:
			input = 0.10
		default:
			input = 0.25
		}
		giftMoney += input
		fmt.Printf("Putting %4.2f into the box, you now have $%5.2f saved\n", input, giftMoney)
	}

}
