package main

import (
	"bufio"
	"fmt"
	"os"
	"sync"
)

// Visited tracks whether web pages have been visited
// Its methods may be used concurrently from multiple goroutines
type Visited struct {
	mu      sync.Mutex
	visited map[string]int
}

func (v *Visited) VisitLink(url string, wg *sync.WaitGroup) int {
	v.mu.Lock()
	defer v.mu.Unlock()
	defer wg.Done()
	count := v.visited[url]
	count++
	v.visited[url] = count
	return count
}

func readLinkList(filename string) []string {
	f, err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanLines)

	urlCount := 0

	urlList := make([]string, 0, 50)
	for scanner.Scan() {
		url := scanner.Text()
		urlCount++
		urlList = append(urlList, url)
	}
	fmt.Printf("Read %d items to url list.\n", urlCount)
	return urlList
}

func main() {
	urls := readLinkList("links.txt")
	fmt.Printf("We have %d urls in the list\n", len(urls))

	visitedUrl := Visited{
		visited: make(map[string]int),
	}

	var wg sync.WaitGroup

	for i := 0; i < len(urls); i++ {
		wg.Add(1)
		go visitedUrl.VisitLink(urls[i], &wg)
	}
	//time.Sleep(10 * time.Second)
	wg.Wait()
	for v := range visitedUrl.visited {
		if visitedUrl.visited[v] > 1 {
			url := v
			if len(v) > 50 {
				url = v[:50]
			}
			fmt.Printf("%50s \t%d\n", url, visitedUrl.visited[v])
		}
	}
}
