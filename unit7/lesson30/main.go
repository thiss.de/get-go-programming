package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

func main() {
	pipeliningChannels(true)
	impatientlyWaiting(false)
	communicatedSleep(false)
	runSleepyGopher(false)
}

// pipeline start
func sourceGopher(downstream chan string) {
	for _, v := range []string{"hello world", "goodby and thanks for all the fish", "don't panic", "where's the bad guy", "a bad apple", "goodbye all"} {
		downstream <- v
	}
	close(downstream)
}

func filterGopher(upstream, downstream chan string) {
	for item := range upstream {
		if !strings.Contains(item, "bad") {
			downstream <- item
		}
	}
	close(downstream)
	/*
		for {
			item, ok := <-upstream
			if !ok {
				close(downstream)
				return
			}
			if !strings.Contains(item, "bad") {
				downstream <- item
			}
		}
	*/
}

func printGopher(upstream chan string) {
	for v := range upstream {
		fmt.Println(v)
	}
	/*
		for {
			v := <-upstream
			if v == "" {
				return
			}
			fmt.Println(v)
		}
	*/
}

func pipeliningChannels(run bool) {
	if !run {
		return
	}
	downstream := make(chan string)
	upstream := make(chan string)
	go sourceGopher(downstream)
	go filterGopher(downstream, upstream)
	go printGopher(upstream)
	time.Sleep(2 * time.Second)
}

// pipeline end

// impatiently waiting start
func impatientlyWaiting(run bool) {
	if !run {
		return
	}
	timeout := time.After(3 * time.Second)
	c := make(chan int)
	for i := 0; i < 5; i++ {
		go sleepyCommunicatingGoper(i, c)
	}
	for i := 0; i < 5; i++ {
		select {
		case gopherId := <-c:
			fmt.Println("gopher", gopherId, "has finished sleeping")
		case <-timeout:
			fmt.Println("my patience ran out")
			return
		}
	}
}

// impatiently waiting end

// sending messages with channels start
func communicatedSleep(run bool) {
	if !run {
		return
	}
	c := make(chan int)
	for i := 0; i < 5; i++ {
		go sleepyCommunicatingGoper(i, c)
	}
	for i := 0; i < 5; i++ {
		gopherId := <-c
		fmt.Println("gopher", gopherId, "has finished sleeping")
	}
}

func sleepyCommunicatingGoper(id int, c chan int) {
	time.Sleep(time.Duration(rand.Intn(4000)) * time.Millisecond)
	fmt.Println("...", id, "snoore...")
	c <- id
}

// sending messages with channels end

// sleepygopher start
func runSleepyGopher(run bool) {
	if !run {
		return
	}
	for i := 0; i < 5; i++ {
		go sleepyGopher(i)
	}
	time.Sleep(4 * time.Second)
}

func sleepyGopher(id int) {
	time.Sleep(3 * time.Second)
	fmt.Println("...", id, "snore ...")
}

// sleepygopher end
