package main

import (
	"image"
	"log"
	"time"
)

type command int

const (
	right = command(0)
	left  = command(1)
	stop  = command(2)
	start = command(3)
)

type RoverDriver struct {
	commandc chan command
}

// Left turns the rover left (90º counterclockwise)
func (r *RoverDriver) Left() {
	r.commandc <- left
}

// Right turns the rover right (90º clockwise)
func (r *RoverDriver) Right() {
	r.commandc <- right
}

// Stop the rover
func (r *RoverDriver) Stop() {
	r.commandc <- stop
}

// Start the rover
func (r *RoverDriver) Start() {
	r.commandc <- start
}

func NewRoverDriver() *RoverDriver {
	r := &RoverDriver{
		commandc: make(chan command),
	}
	go r.drive()
	return r
}

func (r *RoverDriver) drive() {
	pos := image.Point{X: 0, Y: 0}
	direction := image.Point{X: 1, Y: 0}
	updateInterval := 250 * time.Millisecond
	nextMove := time.After(updateInterval)
	speed := 1
	for {
		select {
		case c := <-r.commandc:
			switch c {
			case left:
				direction = image.Point{
					X: -direction.Y,
					Y: direction.X,
				}
			case right:
				direction = image.Point{
					X: direction.Y,
					Y: -direction.X,
				}
			case stop:
				speed = 0
			case start:
				speed = 1
			}
			log.Printf("new direction %v", direction)
		case <-nextMove:
			pos = pos.Add(direction.Mul(speed))
			log.Printf("moved to %v", pos)
			//updateInterval += 500 * time.Millisecond
			nextMove = time.After(updateInterval)
		}
	}
}

//func worker() {
//	pos := image.Point{X: 10, Y: 10}
//	direction := image.Point{X: 1, Y: 0}
//	next := time.After(time.Second)
//	for {
//		select {
//		case <-next:
//			pos = pos.Add(direction)
//			fmt.Println("current position is", pos)
//			next = time.After(time.Second)
//		}
//	}
//}

func main() {
	//go worker()
	//time.Sleep(3 * time.Second)

	r := NewRoverDriver()
	time.Sleep(3 * time.Second)
	r.Left()
	time.Sleep(3 * time.Second)
	r.Right()
	time.Sleep(3 * time.Second)
	r.Stop()
	time.Sleep(3 * time.Second)
	r.Start()
	time.Sleep(3 * time.Second)
}
