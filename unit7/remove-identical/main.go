package main

import (
	"fmt"
	"strings"
)

func sourceGopher(downstream chan string) {
	for _, v := range []string{"Don't panic", "Don't panic", "Goodbye and thanks for all the fish", "hello world", "hello world"} {
		downstream <- v
	}
	close(downstream)
}

func filterGopher(upstream, downstream chan string) {
	var previousItem string
	for item := range upstream {
		if item != previousItem {
			for _, word := range strings.Fields(item) {
				downstream <- word
			}
			previousItem = item
		}
	}
	close(downstream)
}

func printGopher(upstream chan string) {
	for v := range upstream {
		fmt.Println(v)
	}
}

func main() {
	downstream := make(chan string)
	upstream := make(chan string)
	go sourceGopher(downstream)
	go filterGopher(downstream, upstream)
	printGopher(upstream)
}
