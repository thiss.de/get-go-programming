package main

import (
	"fmt"
)

type report struct {
	sol
	temperature // struct embedding
	location    // struct embedding
}

func (r report) days(s2 sol) int {
	return r.sol.days(s2)
}

type sol int

func (s sol) days(s2 sol) int {
	days := int(s2 - s)
	if days < 0 {
		days = -days
	}
	return days
}

type temperature struct {
	high, low celsius
}

func (t temperature) average() celsius {
	return (t.high + t.low) / 2
}

type location struct {
	lat, long float64
}

type celsius float64

func bradburyReport(run bool) {
	if !run {
		return
	}
	bradury := location{-4.5895, 137.4417}
	t := temperature{high: -1.0, low: -78.0}

	report := report{sol: 15, temperature: t, location: bradury}

	fmt.Printf("%+v\n", report)

	fmt.Printf("a balmy %vº C\n", report.temperature.high)
	fmt.Printf("a balmy %vº C embedded\n", report.high)

	fmt.Printf("on average %vº C\n", report.temperature.average())
	fmt.Printf("on report average %vº C\n", report.average())

	fmt.Println(report.sol.days(1446))
	fmt.Println(report.days(1446))
}

func (l location) days(l2 location) int {
	return 5
}

func main() {
	bradburyReport(true)
}
