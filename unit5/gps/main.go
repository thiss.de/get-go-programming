package main

import (
	"fmt"
	"math"
)

type location struct {
	name      string
	lat, long float64
}

func (l location) description() string {
	return fmt.Sprintf("%s, latidude: %.2f, longitude: %.2f", l.name, l.lat, l.long)
}

type world struct {
	radius float64
}

type gps struct {
	world
	current     location
	destination location
}

func (w world) distance(currentLocation, destinationLocation location) float64 {
	s1, c1 := math.Sincos(rad(currentLocation.lat))
	s2, c2 := math.Sincos(rad(destinationLocation.lat))
	clong := math.Cos(rad(currentLocation.long - destinationLocation.long))
	return w.radius * math.Acos(s1*s2+c1*c2*clong)
}

func (g gps) distance() float64 {
	return g.world.distance(g.current, g.destination)
}

func (g gps) message() string {
	return fmt.Sprintf("%.2f km remaining from %s to the destination %s",
		g.distance(), g.current.description(), g.destination.description())
}

func rad(deg float64) float64 {
	return deg * math.Pi / 180
}

type rover struct {
	name string
	gps
}

func main() {
	bradburyLanding := location{"Bradbury Landing", -4.5895, 137.4417}
	elysiumPlanitia := location{"Elysium Planitia", 4.5, 135.9}
	mars := world{radius: 3389.5}

	marsGps := gps{mars, bradburyLanding, elysiumPlanitia}

	curiosity := rover{"Curiosity", marsGps}

	fmt.Println(curiosity.message())
}
