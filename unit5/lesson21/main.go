package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
)

func main() {
	sliceStruct(true)
	locationDemo(false)
	curiosityDemo(false)
}

func sliceStruct(run bool) {
	if !run {
		return
	}
	type location struct {
		Name string  `json:"name" xml:"name"`
		Lat  float64 `json:"latitude" xml:"latitude"`
		Long float64 `json:"longitude" xml:"longitude"`
	}

	locations := []location{
		{Name: "Bradbury Landing", Lat: -4.5895, Long: 137.4417},
		{Name: "Columbia Memorial Station", Lat: -14.5684, Long: 175.472636},
		{Name: "Challenger Memorial Station", Lat: -1.9462, Long: 354.4734},
	}
	for _, l := range locations {
		loc, err := json.MarshalIndent(l, "", "\t")
		exitOnError(err)
		//fmt.Printf("%+v\n", l)
		fmt.Println(string(loc))
	}

	bradburyJson, err := json.Marshal(locations[0])
	exitOnError(err)

	fmt.Println(string(bradburyJson))

	bradburyXml, err := xml.MarshalIndent(locations[0], "", "\t")
	exitOnError(err)
	fmt.Println(string(bradburyXml))
}

func exitOnError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func locationDemo(run bool) {
	if !run {
		return
	}
	type location struct {
		lat  float64
		long float64
	}
	var spirit location
	spirit.lat = -14.5684
	spirit.long = 175.472636

	var opportunity location
	opportunity.lat = -1.9462
	opportunity.long = 354.4734

	insight := location{lat: 4.5, long: 135.9}

	curiosity := location{-4.5895, 137.4417}

	fmt.Printf("%+v\n%+v\n%+v\n%+v\n", spirit, opportunity, insight, curiosity)
}

func curiosityDemo(run bool) {
	if !run {
		return
	}
	var curiosity struct {
		lat  float64
		long float64
	}
	curiosity.lat = -4.5895
	curiosity.long = 137.4417

	fmt.Println(curiosity.lat, curiosity.long)
	fmt.Println(curiosity)
}
