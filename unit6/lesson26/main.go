package main

import (
	"fmt"
	"strings"
	"time"
)

func main() {
	interfacePointers(true)
	modifyPlanets(false)
	chessboarding(false)
	interiorPointers(false)
	days(false)
	havingBirthday(false)
	pointingStructures(false)
	nasa(false)
	home(false)
	initialPointer(false)
}

// start pointers and interfaces
type talker interface {
	talk() string
}

func shout(t talker) {
	louder := strings.ToUpper(t.talk())
	fmt.Println(louder)
}

type martian struct{}

func (m martian) talk() string {
	return "nack nack"
}

type laser int

func (l *laser) talk() string {
	return strings.Repeat("pew ", int(*l))
}

func interfacePointers(run bool) {
	if !run {
		return
	}
	shout(martian{})
	shout(&martian{})

	pew := laser(3)
	shout(&pew)
}

// end pointers and interfaces

// start modify a slice
func reclassify(planets *[]string) {
	*planets = (*planets)[0:8]
}

func modifyPlanets(run bool) {
	if !run {
		return
	}
	planets := []string{
		"Mercury", "Venus", "Earth", "Mars",
		"Jupiter", "Saturn", "Uranus", "Neptune",
		"Pluto",
	}

	reclassify(&planets)
	fmt.Println(planets)
}

// end modify a slice

// start mutating arrays
type chessboard [8][8]rune

func (board *chessboard) reset() {
	board[0][0] = 'r'
	board[0][1] = 'n'
	board[0][2] = 'b'
	board[0][3] = 'k'
	board[0][4] = 'q'
	board[0][5] = 'b'
	board[0][6] = 'n'
	board[0][7] = 'r'

	board[7][0] = 'R'
	board[7][1] = 'N'
	board[7][2] = 'B'
	board[7][3] = 'K'
	board[7][4] = 'Q'
	board[7][5] = 'B'
	board[7][6] = 'N'
	board[7][7] = 'R'

	for column := range board[1] {
		board[1][column] = 'p'
		board[6][column] = 'P'
	}
}

func (board chessboard) show() {
	for i := range board[0] {
		for j := range board[i] {
			element := board[i][j]
			if element == 0 {
				element = '-'
			}
			fmt.Printf("%c", element)
		}
		fmt.Println()
	}
}

func chessboarding(run bool) {
	if !run {
		return
	}
	var c chessboard
	c.reset()
	c.show()
}

// end mutating arrays

// start interior pointers
type stats struct {
	level             int
	endurance, health int
}

func levelUp(s *stats) {
	s.level++
	s.endurance = 42 + (14 * s.level)
	s.health = 5 * s.endurance
}

type character struct {
	name  string
	stats stats
}

func interiorPointers(run bool) {
	if !run {
		return
	}
	player := character{name: "Matthias"}

	// getting a pointer to a struct field
	levelUp(&player.stats)

	fmt.Printf("%+v\n", player.stats)
}

// end interior pointeres

func days(run bool) {
	if !run {
		return
	}
	const layout = "Monday, January 2, 2006"
	day := time.Now()
	tomorrow := day.Add(24 * time.Hour)

	fmt.Println(day.Format(layout))
	fmt.Println(tomorrow.Format(layout))
}

// start mutating
type person struct {
	name, superpower string
	age              int
}

func birthday(p *person) {
	p.age++
}

func havingBirthday(run bool) {
	if !run {
		return
	}
	rebecca := person{
		name:       "Rebecca",
		superpower: "imagination",
		age:        14,
	}

	fmt.Printf("%+v\n", rebecca)
	birthday(&rebecca)
	fmt.Printf("%+v\n", rebecca)
	rebecca.birthday()
	fmt.Printf("%+v\n", rebecca)

	// when a method receiver doesn't get a pointer
	// it can't modify field values
	rebecca.gettingOlder()
	fmt.Printf("%+v\n", rebecca)
}

func (p *person) birthday() {
	p.age++
}

// when a method receiver doesn't get a pointer
// it can't modify field values
func (p person) gettingOlder() {
	p.age++
}

// end mutating

func pointingStructures(run bool) {
	if !run {
		return
	}
	timmy := &person{
		name: "Timothy",
		age:  10,
	}
	(*timmy).superpower = "flying"
	fmt.Printf("%T %+v\n", timmy, timmy)

	timmy.superpower = "walk through walls"
	fmt.Printf("%T %+v\n", timmy, timmy)

	fmt.Printf("%T %+v\n", *timmy, *timmy)

	superpowers := &[3]string{"flight", "invisibility", "super strength"}
	fmt.Println(superpowers[0])
	fmt.Println(superpowers[1:2])
}

func nasa(run bool) {
	if !run {
		return
	}
	var administrator *string

	scolese := "Christopher J. Scolese"

	fmt.Printf("address of scolese '%v' with value \"%v\"\n", &scolese, scolese)

	administrator = &scolese
	fmt.Printf("address of administrator: '%v' with value \"%v\"\n", administrator, *administrator)

	bolden := "Charles F. Bolden"
	fmt.Printf("address of bolden '%v' with value \"%v\"\n", &bolden, bolden)

	administrator = &bolden
	fmt.Printf("address of administrator: '%v' with value \"%v\"\n", administrator, *administrator)

	bolden = "Charles Frank Bolden"
	fmt.Printf("address of administrator: '%v' with value \"%v\"\n", administrator, *administrator)

	*administrator = "Maj. Gen. Charles Frank Bolden Jr."
	fmt.Printf("address of bolden '%v' with value \"%v\"\n", &bolden, bolden)

	major := administrator
	*major = "Major Genneral Charles Frank Bolden Jr."
	fmt.Printf("address of bolden '%v' with value \"%v\"\n", &bolden, bolden)
	fmt.Println(major == administrator)

	lightfoot := "Robert M. Lightfoot Jr."
	administrator = &lightfoot
	fmt.Println(administrator == major)
	fmt.Printf("address of administrator: '%v' with value \"%v\"\n", administrator, *administrator)

	charles := *major
	*major = "Charles Bolden"
	fmt.Println(charles) // dereferenced value of major pointer, which is bolden
	fmt.Println(bolden)  // modified value of major pointer to bolden

	charles = "Charles Bolden"
	fmt.Println(charles == bolden)
	fmt.Println(&charles == &bolden)
}

func home(run bool) {
	if !run {
		return
	}
	canada := "Canada"

	var home *string
	fmt.Printf("home is a %T\n", home)

	home = &canada
	fmt.Println(*home)
}

func initialPointer(run bool) {
	if !run {
		return
	}
	answer := 42
	fmt.Println(&answer)

	address := &answer
	fmt.Printf("address is of type %T with value %v\n", address, *address)
}
