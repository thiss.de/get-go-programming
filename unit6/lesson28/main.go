package main

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"net/url"
)

func main() {
	urlErrors(true)
	keepCalm(false)
	demoSudoku(false)
	safeProverbs(false)
	callProverbs(false)
	readFiles(false)
}

// url error start
func urlErrors(run bool) {
	if !run {
		return
	}
	u, err := url.Parse("https://exa mple.org")
	if err != nil {
		fmt.Println(err)
		fmt.Printf("%#v\n", err)
		
		if e, ok := err.(*url.Error); ok {
			fmt.Println("Op:", e.Op)
			fmt.Println("URL:", e.URL)
			fmt.Println("Err:", e.Err)
		}
		os.Exit(1)
	}
	fmt.Println(u)
}
// url error end

// panic: keep calm and carry on start
func keepCalm(run bool) {
	if !run {
		return
	}
	defer func() {
		if e := recover(); e != nil {
			fmt.Println(e)
		}
	}()
	panic("I forgot my towel")
}
// panic: keep calm and carry on end

// custom errors start
type error interface {
	Error() string
}

type SudokuError []error

func (se SudokuError) Error() string {
	var s []string
	for _, err := range se {
		s = append(s, err.Error())
	}
	return strings.Join(s, ", ")
}

func (g *Grid) SafeSet(row, column int, digit int8) error {
	var errs SudokuError
	if !inBounds(row, column) {
		errs = append(errs, ErrBounds)
	}
	if !validDigit(digit) {
		errs = append(errs, ErrDigit)
	}
	if len(errs) > 0 {
		return errs
	}
	g[row][column] = digit
	return nil
}

// custom errors end

// sudoku errors start
const rows, columns = 9, 9

type Grid [rows][columns]int8

var (
	ErrBounds = errors.New("out of bounds")
	ErrDigit  = errors.New("invalid digit")
)

func (g *Grid) Set(row, column int, digit int8) error {
	if !inBounds(row, column) {
		return ErrBounds
	}
	if !validDigit(digit) {
		return ErrDigit
	}
	g[row][column] = digit
	return nil
}

func inBounds(row, column int) bool {
	if row < 0 || row >= rows {
		return false
	}
	if column < 0 || column >= columns {
		return false
	}
	return true
}

func validDigit(digit int8) bool {
	return digit >= 1 && digit <= 9
}

func demoSudoku(run bool) {
	if !run {
		return
	}
	var g Grid
	err := g.Set(3, 0, 5)
	if err != nil {
		switch err {
		case ErrBounds, ErrDigit:
			fmt.Println("Error, the parameters are out of limit.")
		default:
			fmt.Printf("An error occured: %v.\n", err)
		}
		os.Exit(1)
	}

	err = g.SafeSet(10, 0, 42)
	if err != nil {
		if errs, ok := err.(SudokuError); ok {
			fmt.Printf("%d error(s) occured:\n", len(errs))
			for _, e := range errs {
				fmt.Printf("- %v\n", e)
			}
		}
		os.Exit(1)
	}
}

// sudoku errors end

// creative error handling start
type safeWriter struct {
	w   io.Writer
	err error
}

func (sw *safeWriter) writeln(s string) {
	if sw.err != nil {
		return
	}

	_, sw.err = fmt.Fprintln(sw.w, s)
}

func safeProverbs(run bool) error {
	if !run {
		return nil
	}
	name := "proverbs.txt"
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	defer f.Close()

	proverbs := []string{
		"Errors are values.",
		"Don’t just check errors, handle them gracefully.",
		"Don’t panic.",
		"Make the zero value useful.",
		"The bigger the interface, the weaker the abstraction.",
		"interface{} says nothing.",
		"Gofmt’s style is no one’s favorite, yet gofmt is everyone’s favorite. Documentation is for users.",
		"A little copying is better than a little dependency.",
		"Clear is better than clever.",
		"Concurrency is not parallelism.",
		"Don’t communicate by sharing memory, share memory by communicating.",
	}

	sw := safeWriter{w: f}
	for _, v := range proverbs {
		sw.writeln(v)
	}
	return sw.err
}

// creative error handling end

// handling errors start
func proverbs(name string) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	defer f.Close()

	proverbs := []string{
		"Errors are values.",
		"Don’t just check errors, handle them gracefully.",
		"Don’t panic.",
		"Make the zero value useful.",
		"The bigger the interface, the weaker the abstraction.",
		"interface{} says nothing.",
		"Gofmt’s style is no one’s favorite, yet gofmt is everyone’s favorite. Documentation is for users.",
		"A little copying is better than a little dependency.",
		"Clear is better than clever.",
		"Concurrency is not parallelism.",
		"Don’t communicate by sharing memory, share memory by communicating.",
	}

	for _, v := range proverbs {
		_, err = fmt.Fprintln(f, v)
		if err != nil {
			return err
		}
	}

	_, err = fmt.Fprintf(f, "Channels orchestrate; mutexes serialize.")
	return err
}

func callProverbs(run bool) {
	if !run {
		return
	}
	err := proverbs("proverbs.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// handling errors end

func readFiles(run bool) {
	if !run {
		return
	}
	files, err := ioutil.ReadDir(".")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for _, file := range files {
		fmt.Println(file.Name())
	}
}
