package main

import (
	"fmt"
	"sort"
)

func main() {
	makeNumbers(true)
	nilInterfaces(false)
	nilMaps(false)
	nilSlices(false)
	sortedFruits(false)
	nobodyHasBirthday(false)
	lookIntoTheVoid(false)
}

// alternative to nil start
type number struct {
	value int
	valid bool
}

func newNumber(v int) number {
	return number{value: v, valid: true}
}
func (n number) String() string {
	if !n.valid {
		return "not set"
	}
	return fmt.Sprintf("%d", n.value)
}

func makeNumbers(run bool) {
	if !run {
		return
	}
	var n number
	fmt.Println(n)

	n = newNumber(5)
	fmt.Println(n)

}

// alternative to nil end

// nil interfaces
func nilInterfaces(run bool) {
	if !run {
		return
	}
	var v interface{}
	fmt.Printf("%T %v %v\n", v, v, v == nil)
	fmt.Printf("%#v\n", v)

	var p *int
	v = p
	fmt.Printf("%T %v %v\n", v, v, v == nil)
	fmt.Printf("%#v\n", v)
}

// nip maps start
func nilMaps(run bool) {
	if !run {
		return
	}
	var soup map[string]int
	fmt.Println(soup == nil)

	measurement, ok := soup["onion"]
	if ok {
		fmt.Println(measurement)
	}
	for ingredient, measurement := range soup {
		fmt.Println(ingredient, measurement)
	}
}

// nip maps end

// nil slices start
func mirepoix(ingredients []string) []string {
	return append(ingredients, "onion", "carrot", "celery")
}

func nilSlices(run bool) {
	if !run {
		return
	}
	var soup []string
	fmt.Println(soup == nil)

	fmt.Println(len(soup))

	soup = append(soup, "onion", "carrot", "celery")
	fmt.Println(soup)

	veggieSoup := mirepoix(nil)
	fmt.Println(veggieSoup)
}

// nil slices end

// start function pointers
func sortStrings(s []string, less func(i, j int) bool) {
	if less == nil {
		less = func(i, j int) bool { return s[i] < s[j] }
	}
	sort.Slice(s, less)
}

func sortedFruits(run bool) {
	if !run {
		return
	}
	food := []string{"onion", "carrot", "celery", "apple", "pear"}
	sortStrings(food, nil)
	fmt.Println(food)

	sortStrings(food, func(i, j int) bool { return len(food[i]) < len(food[j]) })
	fmt.Println(food)

	sortStrings(food, func(i, j int) bool { return len(food[i]) > len(food[j]) })
	fmt.Println(food)
}

// end function pointers

// start nil receivers
type person struct {
	age int
}

func (p *person) birthday() {
	if p == nil {
		return
	}
	p.age++
}

func nobodyHasBirthday(run bool) {
	if !run {
		return
	}
	var nobody *person
	fmt.Println(nobody)
	nobody.birthday()

	somebody := person{age: 41}
	somebody.birthday()
	fmt.Println(somebody)
}

// end nil receivers

func lookIntoTheVoid(run bool) {
	if !run {
		return
	}
	var nowhere *int
	fmt.Println(nowhere)

	// to see panic in action, run the next line
	//fmt.Println(*nowhere)
	if nowhere != nil {
		fmt.Println(*nowhere)
	}

	var nothing *string
	fmt.Println(nothing)
}
