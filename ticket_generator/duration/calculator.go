package duration

import (
	"math/rand"
	"time"
)

var speed int

func TravelTime() int {
	t := time.Now()
	rand.Seed(int64(t.Nanosecond()))

	const secondsPerDay = 60 * 60 * 24

	speed = rand.Intn(15) + 16 // km/s
	var distance = 62100000    // km

	return int(distance / speed / secondsPerDay)
}

func GetPrice(tripType string) int {
	var price = 20 + speed

	switch tripType {
	case "One-way":
		return price
	default:
		return price * 2
	}
}
