package main

import (
	"fmt"
	"math/rand"
	"time"

	"thiss.de/ticket_generator/duration"
)

func main() {
	t := time.Now()
	rand.Seed(int64(t.Nanosecond()))

	fmt.Printf("")

	var destination string
	var ticketType string
	var days int

	fmt.Printf("%-18v %4v %-11v %3v\n", "Spaceline", "Days", "Trip type", "Price")
	for head := 0; head < 41; head++ {
		fmt.Print("=")
	}
	fmt.Println()

	for count := 0; count < 10; count++ {

		switch num := rand.Intn(3); num {
		case 0:
			destination = "Space Adventures"
		case 1:
			destination = "SpaceX"
		default:
			destination = "Virgin Galactic"
		}

		switch num := rand.Intn(2); num {
		case 0:
			ticketType = "One-way"
		default:
			ticketType = "Round-trip"
		}

		days = duration.TravelTime()

		fmt.Printf("%-18v %4d %-11v $%3d\n", destination, days, ticketType, duration.GetPrice(ticketType))
	}
}
