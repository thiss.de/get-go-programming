package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

const (
	width  = 80
	height = 15
)

type Universe [][]bool

func NewUniverse() Universe {
	u := make(Universe, height)
	for i := range u {
		u[i] = make([]bool, width)
	}
	return u
}

func (u Universe) String() string {
	var b byte
	buf := make([]byte, 0, (width+1)*height)
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			b = '-'
			if u[y][x] {
				b = '*'
			}
			buf = append(buf, b)
		}
		buf = append(buf, '\n')
	}
	return string(buf)
}

func (u Universe) Show() {
	/*
		for i := range u {
			for j := range u[i] {
				c := "-"
				if u[i][j] {
					c = "*"
				}
				fmt.Print(c)
			}
			fmt.Println()
		}
	*/
	fmt.Print("\033[H", u.String())
}

// Seed sets approximately 25% of the cells to live
func (u Universe) Seed() {
	//t := time.Now()
	//rand.Seed(int64(t.Nanosecond()))
	livingCellCount := int(math.Trunc(width * height / 4))
	for i := 0; i < livingCellCount; i++ {
		row := rand.Intn(height)
		column := rand.Intn(width)
		u[row][column] = true
	}
}

func (u Universe) Set(x, y int, b bool) {
	u[y][x] = b
}

// Game rules
func (u Universe) Alive(x, y int) bool {
	//	fmt.Printf("\tchecking cell [%d, %d] is alive. ", x, y)
	/*
		if x >= height {
			x = x % height
		} else if x < 0 {
			x = height + x
		}
		if y >= width {
			y = y % width
		} else if y < 0 {
			y = width + y
		}
	*/
	// Same logic shorter
	x = (x + width) % width
	y = (y + height) % height
	//	fmt.Printf("adjusted to cell [%d, %d]\n", x, y)
	return u[y][x]
}

// Neighbors counts the number of how many of the 8 neighobrs are living
func (u Universe) Neighbors(x, y int) int {
	//	fmt.Printf("Checking living neighbors for cell [%d, %d]\n", x, y)
	livingNeighbors := 0
	for row := -1; row <= 1; row++ {
		for col := -1; col <= 1; col++ {
			/*
				if row == 0 && col == 0 {
					continue
				}
				xNeighbor := x + col
				yNeighbor := y + row
				if u.Alive(xNeighbor, yNeighbor) {
					//				fmt.Printf("Neighbor [%d, %d] is alive\n", xNeighbor, yNeighbor)
					livingNeighbors++
				}
			*/
			if !(row == 0 && col == 0) && u.Alive(x+row, y+col) {
				livingNeighbors++
			}
		}
	}
	return livingNeighbors
}

func (u Universe) Next(x, y int) bool {
	livingNeighbors := u.Neighbors(x, y)
	/*
		switch {
		case livingNeighbors < 2:
			return false
		case livingNeighbors == 2 || livingNeighbors == 3:
			return true
		default:
			return false
		}
	*/
	return livingNeighbors == 3 || livingNeighbors == 2 && u.Alive(x, y)
}

func Step(a, b Universe) Universe {
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			b.Set(x, y, a.Next(x, y))
		}
	}
	return b
}

func main() {
	a := NewUniverse()
	b := NewUniverse()
	a.Seed()
	generations := 600
	for i := 0; i < generations; i++ {
		Step(a, b)
		a.Show()
		time.Sleep(time.Second / 30)
		a, b = b, a
	}

	//x := 0
	//y := 19
	//fmt.Printf("Cell [%d,%d] has %d living neighbors\n", x, y, a.Neighbors(x, y))
	/*
		x = 15
		y = 80
		fmt.Printf("Cell [%d,%d] has %d living neighbors\n", x, y, universe.Neighbors(x, y))
		x = 0
		y = 0
		fmt.Printf("Cell [%d,%d] has %d living neighbors\n", x, y, universe.Neighbors(x, y))
		x = 16
		y = 81
		fmt.Printf("Cell [%d,%d] has %d living neighbors\n", x, y, universe.Neighbors(x, y))
	*/
}
