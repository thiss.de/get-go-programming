package main

import (
	"fmt"
	"math"
	"sort"
	"strings"
)

func main() {
	wordCount(true)
	simulateSet(false)
	grouping(false)
	frequency(false)
	whoops(false)
	minimalMap(false)
}

func wordCount(run bool) {
	if !run {
		return
	}
	text := `
As far as eye could reach he saw nothing but the stems of the great plants about him receding in the violet shade, and far overhead the multiple transparency of huge leaves filtering the sunshine to the solemn splendour of twilight in which he walked. Whenever he felt able he ran again; the ground continued soft and springy, covered with the same resilient weed which was the first thing his hands had touched in Malacandra. Once or twice a small red creature scuttled across his path, but otherwise there seemed to be no life stirring in the wood; nothing to fear—except the fact of wandering unprovisioned and alone in a forest of unknown vegetation thousands or millions of miles beyond the reach or knowledge of man.`
	textFields := strings.Fields(text)

	fmt.Printf("%T \n%v\n", textFields, len(textFields))

	for i := range textFields {
		textFields[i] = strings.ToLower(strings.Trim(textFields[i], ",.;"))
	}

	countWords := make(map[string]int)

	for _, v := range textFields {
		countWords[v]++
	}

	for word, count := range countWords {
		if count > 1 {
			fmt.Printf("the word \"%s\" appears %d times\n", word, count)
		}
	}
}

func simulateSet(run bool) {
	if !run {
		return
	}
	var temperatures = []float64{
		-28.0, 32.0, -31.0, -29.0, -23.0, -29.0, -28.0, -33.0,
	}

	set := make(map[float64]bool)

	for _, t := range temperatures {
		set[t] = true
	}
	if set[-28.0] {
		fmt.Println("set member")
	}
	fmt.Println(set)

	unique := make([]float64, 0, len(set))
	for t := range set {
		unique = append(unique, t)
	}
	sort.Float64s(unique)
	fmt.Println(unique)
}

func grouping(run bool) {
	if !run {
		return
	}
	temperatures := []float64{
		-28.0, 32.0, -31.0, -29.0, -23.0, -29.0, -28.0, -33.0,
	}
	groups := make(map[float64][]float64)

	for _, t := range temperatures {
		g := math.Trunc(t/10) * 10
		groups[g] = append(groups[g], t)
	}

	for g, temperatures := range groups {
		fmt.Printf("%v: %v\n", g, temperatures)
	}
}

func frequency(run bool) {
	if !run {
		return
	}
	temperatures := []float64{
		-28.0, 32.0, -31.0, -29.0, -23.0, -29.0, -28.0, -33.0,
	}
	frequency := make(map[float64]int)

	for _, t := range temperatures {
		frequency[t]++
	}

	for t, num := range frequency {
		fmt.Printf("%+2.f occurs %d times.\n", t, num)
	}
}

func whoops(run bool) {
	if !run {
		return
	}

	planets := map[string]string{
		"Earth": "Sector ZZ9",
		"Mars":  "Sector ZZ9",
	}
	planetsMarkII := planets
	planets["Earth"] = "whoops"

	fmt.Println(planets)
	fmt.Println(planetsMarkII)

	delete(planets, "Earth")
	fmt.Println(planetsMarkII)
}

func minimalMap(run bool) {
	if !run {
		return
	}

	temperature := map[string]int{
		"Earth": 15,
		"Mars":  -65,
	}
	temp := temperature["Earth"]
	fmt.Printf("On average the Earth is %vºC warm.\n", temp)

	temperature["Earth"] = 16
	temperature["Venus"] = 464
	fmt.Println(temperature)

	moon1 := temperature["Moon"]
	fmt.Println(moon1)

	if moon, ok := temperature["Moon"]; ok {
		fmt.Printf("On average the moon is %vºC warm.\n", moon)
	} else {
		fmt.Println("Where is the moon?")
	}

	temperature["Moon"] = -54
	if moon, ok := temperature["Moon"]; ok {
		fmt.Printf("On average the moon is %vºC warm.\n", moon)
	} else {
		fmt.Println("Where is the moon?")
	}
}
