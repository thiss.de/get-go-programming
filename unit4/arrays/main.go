package main

import (
	"fmt"
	"sort"
	"strings"
)

func main() {
	terraformning(true)
	sortingSlices(false)
	runHyperspace(false)
	slicingDeeper(false)
	slicingAnArray(false)
	arrayOfArrays(false)
	arrayExample(false)
}

type Planets []string

func terraformning(run bool) {
	if !run {
		return
	}
	planets := []string{"Mars", "Uranus", "Neptun"}

	fmt.Println(planets)
	Planets(planets).terraform()
	fmt.Println(planets)
}

func (planets Planets) terraform() {
	for i, p := range planets {
		planets[i] = "New " + p
	}
}

func sortingSlices(run bool) {
	if !run {
		return
	}
	planets := []string{
		"Mercury",
		"Venus",
		"Earth",
		"Mars",
		"Jupiter",
		"Saturn",
		"Uranus",
		"Neptun",
		"Pluto",
	}
	sort.StringSlice(planets).Sort()
	fmt.Println(planets)
	sort.Strings(planets)
}

func runHyperspace(run bool) {
	if !run {
		return
	}
	planets := []string{"    Venus    ", "Earth      ", "    Mars"}
	hyperspace(planets)

	fmt.Println(strings.Join(planets, ", "))
}

func hyperspace(worlds []string) {
	for i := range worlds {
		worlds[i] = strings.TrimSpace(worlds[i])
	}
}

func slicingDeeper(run bool) {
	if !run {
		return
	}
	dwarfs := [5]string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}
	dwarfSlice := dwarfs[:]

	slicedDwarfs := []string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}

	fmt.Printf("dwarfs: %T, dwarfsSlice: %T, slicedDwarfs: %T\n", dwarfs, dwarfSlice, slicedDwarfs)
}

func slicingAnArray(run bool) {
	if !run {
		return
	}

	planets := [...]string{
		"Mercury",
		"Venus",
		"Earth",
		"Mars",
		"Jupiter",
		"Saturn",
		"Uranus",
		"Neptun",
		"Pluto",
	}

	terrestrial := planets[0:4]
	gasGiants := planets[4:6]
	iceGiants := planets[6:9]
	fmt.Println(terrestrial, gasGiants, iceGiants)

	giants := planets[4:8]
	gas := giants[0:2]
	ice := giants[2:4]

	fmt.Println(giants, gas, ice)

	iceGiantsMarkII := iceGiants
	iceGiants[1] = "Poseidon"
	fmt.Println(planets)
	fmt.Println(iceGiants, iceGiantsMarkII, ice)

	neptune := "Neptune"
	tune := neptune[3:]
	fmt.Println(tune)

	fmt.Println(planets[2:4])
	fmt.Println(terrestrial[2:])
}

func arrayOfArrays(run bool) {
	if !run {
		return
	}

	var board [8][8]rune
	board[0][0] = 'r'
	board[0][1] = 'n'
	board[0][2] = 'b'
	board[0][3] = 'k'
	board[0][4] = 'q'
	board[0][5] = 'b'
	board[0][6] = 'n'
	board[0][7] = 'r'

	board[7][0] = 'R'
	board[7][1] = 'N'
	board[7][2] = 'B'
	board[7][3] = 'K'
	board[7][4] = 'Q'
	board[7][5] = 'B'
	board[7][6] = 'N'
	board[7][7] = 'R'

	for column := range board[1] {
		board[1][column] = 'p'
		board[6][column] = 'P'
	}

	for i := range board[0] {
		for j := range board[i] {
			element := board[i][j]
			if element == 0 {
				element = '-'
			}
			fmt.Printf("%c", element)
		}
		fmt.Println()
	}
}

func arrayExample(run bool) {
	if !run {
		return
	}

	var planets [9]string

	planets[0] = "Mercury"
	planets[1] = "Venus"
	planets[2] = "Earth"
	planets[3] = "Mars"
	planets[4] = "Jupiter"
	planets[5] = "Saturn"
	planets[6] = "Uranus"
	planets[7] = "Neptun"
	planets[8] = "Pluto"

	earth := planets[2]
	fmt.Println(earth)

	fmt.Println(len(planets))
	fmt.Println(planets[3] == "")

	var numbers [3]int

	fmt.Println(numbers[0])

	dwarfs := [5]string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}
	fmt.Println(len(dwarfs))

	allPlanets := [...]string{
		"Mercury",
		"Venus",
		"Earth",
		"Mars",
		"Jupiter",
		"Saturn",
		"Uranus",
		"Neptun",
		"Pluto",
	}
	fmt.Println(len(allPlanets))

	for i := 0; i < len(allPlanets); i++ {
		planet := allPlanets[i]
		fmt.Printf("Planet number %d is %s\n", i+1, planet)
	}

	for i, dwarf := range dwarfs {
		fmt.Printf("Dwarf number %d is %s\n", i+1, dwarf)
	}

	planetsMarkII := allPlanets
	planetsMarkII[2] = "whoops"

	fmt.Println(allPlanets)
	fmt.Println(planetsMarkII)
}
