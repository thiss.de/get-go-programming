package main

import (
	"fmt"
	"strconv"
)

func main() {
	appendedCapacity(true)
	variadicTerraform(false)
	threeIndexSlicing(false)
	lengthAndCapacity(false)
	appending(false)
}

func appendedCapacity(run bool) {
	if !run {
		return
	}

	items := make([]string, 2)

	capacity := cap(items)
	for i := 0; i < 300; i++ {
		items = append(items, "item"+strconv.Itoa(i))
		if cap(items) > capacity {
			fmt.Printf("capacity changed from %d to %d\n", capacity, cap(items))
			capacity = cap(items)
		}
	}
}

func variadicTerraform(run bool) {
	if !run {
		return
	}

	twoWorlds := terraform("New", "Venus", "Mars")
	fmt.Println(twoWorlds)

	planets := []string{"Venus", "Mars", "Jupiter"}
	newPlanets := terraform("New", planets...)
	fmt.Println(newPlanets)
}

func terraform(prefix string, worlds ...string) []string {
	newWorlds := make([]string, len(worlds))

	for i := range worlds {
		newWorlds[i] = prefix + " " + worlds[i]
	}
	return newWorlds
}

func threeIndexSlicing(run bool) {
	if !run {
		return
	}
	planets := []string{
		"Mercury",
		"Venus",
		"Earth",
		"Mars",
		"Jupiter",
		"Saturn",
		"Uranus",
		"Neptun",
		"Pluto",
	}

	terrestrial := planets[0:4:4]
	worlds := append(terrestrial, "Ceres")
	dump("terrestrial", terrestrial)
	dump("worlds", worlds)
	dump("planets", planets)

	terrestrial2 := planets[0:4]
	worlds2 := append(terrestrial2, "Ceres")
	dump("terrestrial2", terrestrial2)
	dump("worlds2", worlds2)
	dump("planets", planets)
}

func lengthAndCapacity(run bool) {
	if !run {
		return
	}
	dwarfs := []string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}
	dump("dwarfs", dwarfs)
	dump("dwarfs[1:2]", dwarfs[1:2])
	dump("dwarfs[2:3]", dwarfs[2:3])
	dwarfs2 := append(dwarfs, "Orcus")
	dwarfs3 := append(dwarfs2, "Salacia", "Quaoar", "Sedna")
	dump("dwarfs2", dwarfs2)
	dump("dwarfs3", dwarfs3)

	dwarfs3[1] = "Pluto!"
	dump("dwarfs[1:2]", dwarfs[1:2])
	dump("dwarfs2[1:2]", dwarfs2[1:2])
	dump("dwarfs3[1:2]", dwarfs3[1:2])
}

func dump(label string, slice []string) {
	fmt.Printf("%v: length %v, capacity %v %v\n", label, len(slice), cap(slice), slice)
}

func appending(run bool) {
	if !run {
		return
	}

	dwarfs := []string{"Ceres", "Pluto", "Haumea", "Makemake", "Eris"}
	dwarfs = append(dwarfs, "Orcus")
	fmt.Printf("%v is type %T with legnth: %d\n", dwarfs, dwarfs, len(dwarfs))

	dwarfs = append(dwarfs, "Salacia", "Quaoar", "Sedna")
	fmt.Printf("%v is type %T with legnth: %d\n", dwarfs, dwarfs, len(dwarfs))
}
