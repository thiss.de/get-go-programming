package main

import (
	"fmt"
	"math/rand"
	"time"
)

type celsius float64
type kelvin float64
type fahrenheit float64
type sensor func() kelvin

func main() {
	convertTemparatures(false)
	newTypes(false)
	convertTemparaturesTyped(false)
	sensorFunc(false)
	functionParameter(false)
	masquerade(false)
	calibrateSensor(true)
	keepReference(false)
}

func keepReference(run bool) {
	if !run {
		return
	}

	var k kelvin = 294.0
	s := func() kelvin {
		return k
	}
	fmt.Println(s())
	k++
	fmt.Println(s())
}

func calibrateSensor(run bool) {
	if !run {
		return
	}
	offset := kelvin(5)
	sensor := calibrate(realSensor, offset)
	offset = kelvin(7)
	fmt.Println(calibrate(realSensor, offset)())
	fmt.Println(sensor())

	measureTemperatureTyped(3, calibrate(fakeSensor, 6))
}

func calibrate(s sensor, offset kelvin) sensor {
	return func() kelvin {
		return s() + offset
	}
}

func masquerade(run bool) {
	if !run {
		return
	}
	var f = func() {
		fmt.Println("I've no name. I'm masqued.")
	}
	f()

	g := func(message string) {
		fmt.Println(message)
	}
	g("Anonymus with parameter")

	func() { fmt.Println("I nearly do not exist.") }()
	func(message string) { fmt.Println("I nearly do not exist.", message) }("and take parameters...")
}

func functionParameter(run bool) {
	if !run {
		return
	}
	measureTemperature(3, fakeSensor)
	measureTemperature(3, realSensor)

	measureTemperatureTyped(3, fakeSensor)
}

func measureTemperatureTyped(samples int, s sensor) {
	for i := 0; i < samples; i++ {
		k := s()
		fmt.Printf("%vºK\n", k)
		time.Sleep(time.Second)
	}
}

func measureTemperature(samples int, sensor func() kelvin) {
	for i := 0; i < samples; i++ {
		k := sensor()
		fmt.Printf("%vºK\n", k)
		time.Sleep(time.Second)
	}
}

func sensorFunc(run bool) {
	if !run {
		return
	}
	sensor := fakeSensor
	fmt.Printf("sensor is of type '%T' and return value %6.2f when called.\n", sensor, sensor())
	sensor = realSensor
	fmt.Printf("sensor is of type '%T' and return value %6.2f when called.\n", sensor, sensor())
}

func fakeSensor() kelvin {
	return kelvin(rand.Intn(151) + 150)
}

func realSensor() kelvin {
	return 0
}

func convertTemparaturesTyped(run bool) {
	if !run {
		return
	}
	var k kelvin = 294.0
	c := kelvinTypeToCelsius(k)
	fmt.Printf("%3.2fº K is %3.2fºC\n", k, c)

	c = 20.85
	k = celsiusTypeToKelvin(c)
	fmt.Printf("%3.2fº C is %3.2fºK\n", c, k)

	k = 294.0
	c = k.celsius()
	fmt.Printf("%3.2fº K is %3.2fºC\n", k, c)

	var f fahrenheit = 64.0
	c = f.celsius()
	fmt.Printf("%3.2fºF is %3.2fºC\n", f, c)

	c = 17.78
	f = c.fahrenheit()
	fmt.Printf("%3.2fºC is %3.2fºF\n", c, f)
}

func (c celsius) fahrenheit() fahrenheit {
	return fahrenheit((c * 9.0 / 5.0) + 32)
}

func (f fahrenheit) celsius() celsius {
	return celsius((f - 32.0) * 5.0 / 9.0)
}

func (k kelvin) celsius() celsius {
	return celsius(k - 273.15)
}

func kelvinTypeToCelsius(k kelvin) celsius {
	return celsius(k - 273.15)
}

func celsiusTypeToKelvin(c celsius) kelvin {
	return kelvin(c + 273.15)
}

func newTypes(run bool) {
	if !run {
		return
	}

	var temperature celsius = 20
	fmt.Printf("%v is of type %T\n", temperature, temperature)

}

func convertTemparatures(run bool) {
	if !run {
		return
	}
	kelvin := 294.0
	celsius := kelvinToCelsius(kelvin)
	fmt.Printf("%3.2fº K is %3.2fºC\n", kelvin, celsius)

	celsius = 18.0
	fmt.Printf("%3.2fºC is %3.2fºF\n", celsius, celsiusToFahrenheit(celsius))

	kelvin = 0.0
	fmt.Printf("%3.2fºC is %3.2fºF\n", kelvin, kelvinToFahrenheit(kelvin))
}

// kelvinToCelsius converts ºK to ºC
func kelvinToCelsius(k float64) float64 {
	k -= 273.15
	return k
}

// celsiusToFahrenheit convert ºC to ºF
func celsiusToFahrenheit(c float64) float64 {
	f := (c * 9.0 / 5.0) + 32
	return f
}

// kelvinToFahrenheit converts ºk to ºF
func kelvinToFahrenheit(k float64) float64 {
	return celsiusToFahrenheit(kelvinToCelsius(k))
}
