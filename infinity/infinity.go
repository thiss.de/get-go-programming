package main

import (
	"fmt"
	"math/rand"
)

func main() {
	var degrees = 0

	var round = 1

	for {

		degrees++
		if degrees >= 360 {
			fmt.Printf("Round %2d\n", round)
			round++
			degrees = 0
			if rand.Intn(2) == 0 {
				break
			}
		}
	}
}
