package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	for count := 10; count > 0; count-- {
		fmt.Print(count, ", ")
	}
	fmt.Println()

	t := time.Now()
	rand.Seed(int64(t.Nanosecond()))

	if num := rand.Intn(3); num == 0 {
		fmt.Println("Space Adventures")
	} else if num == 1 {
		fmt.Println("SpaceX")
	} else {
		fmt.Println("Virgin Galactic")
	}

}
