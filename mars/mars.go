package main

import "fmt"

func main()  {
  fmt.Print("My weight on the surface of Mars is ")
  fmt.Print(78.0 * 0.3783)
  fmt.Print(" kg, and I would be ")
  fmt.Print(53 * 365 / 687)
  fmt.Print(" years old.\n")
  
  fmt.Print("My weight on the surface of Mars is ", 78.0 * 0.3783, 
    " kg, and I would be ", 53 * 365 / 687, " years old.\n")
  
  fmt.Printf("My wieght on the surface of Mars is %v kg, ", 78.0 * 0.3783)
  fmt.Printf("and I would be %v years old.\n", 53 * 365 / 687)
  
  fmt.Printf("%-15v $%4v\n", "SpaceX", 94)
  fmt.Printf("%-15v $%4v\n", "Virgin Galactic", 100)
  
  const lightSpeed = 299792 // km/s
  var distance = 56000000 // km
  
  fmt.Println(distance/lightSpeed, "seconds")
  
  distance = 401000000
  fmt.Println(distance/lightSpeed, "seconds")
}