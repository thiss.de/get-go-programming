package main

import (
				"fmt"
				str "strings"

				"thiss.de/packer/numbers"
				"thiss.de/packer/strings"
				"thiss.de/packer/strings/greeting"
)

func main() {
				fmt.Println(numbers.IsPrime(19))
				fmt.Println(greeting.WelcomeText)
				fmt.Println(strings.Reverse("thorstenthecoder"))
				fmt.Println(str.Count("Go is Awesome. I love Go", "Go"))
}

