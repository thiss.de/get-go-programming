package main

import (
	cRand "crypto/rand"
	"fmt"
	"math/big"
	mrand "math/rand"
)

func main() {

	var upperBound = 100
	var lowerBound = 1
	var rangeOfGuess = upperBound - lowerBound
	var num = mrand.Intn(upperBound) + lowerBound
	var tries = 1

	var randomTarget, err = cRand.Int(cRand.Reader, big.NewInt(100))
	if err != nil {
		panic(err)
	}
	var targetNumber = int(randomTarget.Uint64())

	for {
		if num == targetNumber {
			fmt.Printf("I guessed the number %d correctly, after %d tries\n", targetNumber, tries)
			break
		} else if num < targetNumber {
			fmt.Printf("%d is too small. Trying again...", num)
			lowerBound = num + 1
			rangeOfGuess = upperBound - num
			fmt.Printf("guessing from intervall [%d, %d], range width: %d\n\n",
				lowerBound, upperBound, rangeOfGuess)
			num = mrand.Intn(rangeOfGuess) + lowerBound
		} else {
			fmt.Printf("%d is too great. Trying again...", num)
			rangeOfGuess = num - lowerBound
			upperBound = num
			fmt.Printf("guessing from intervall [%d, %d], range width: %d\n\n",
				lowerBound, upperBound, rangeOfGuess)
			num = mrand.Intn(rangeOfGuess) + lowerBound
		}
		tries++
	}

}
